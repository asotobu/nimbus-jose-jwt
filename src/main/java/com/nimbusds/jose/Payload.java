package com.nimbusds.jose;


import java.nio.charset.Charset;
import java.text.ParseException;

import net.jcip.annotations.Immutable;

import net.minidev.json.JSONObject;

import com.nimbusds.jwt.SignedJWT;

import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jose.util.JSONObjectUtils;


/**
 * Payload with JSON object, string, byte array, Base64URL, JWS object and
 * signed JWT views. Represents the original object that was signed with JWS or
 * encrypted with JWE. This class is immutable.
 *
 * <p>UTF-8 is the character set for all conversions between strings and byte
 * arrays.
 *
 * <p>Conversion relations:
 *
 * <pre>
 * JSONObject <=> String <=> Base64URL
 *                       <=> byte[]
 *                       <=> JWSObject
 *                       <=> SignedJWT
 * </pre>
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2014-10-28)
 */
@Immutable
public final class Payload {


	/**
	 * Enumeration of the original data types used to create a 
	 * {@link Payload}.
	 */
	public static enum Origin {


		/**
		 * The payload was created from a JSON object.
		 */
		JSON,


		/**
		 * The payload was created from a string.
		 */
		STRING,


		/**
		 * The payload was created from a byte array.
		 */
		BYTE_ARRAY,


		/**
		 * The payload was created from a Base64URL-encoded object.
		 */
		BASE64URL,


		/**
		 * The payload was created from a JWS object.
		 */
		JWS_OBJECT,


		/**
		 * The payload was created from a signed JSON Web Token (JWT).
		 */
		SIGNED_JWT
	}


	/**
	 * UTF-8 is the character set for all conversions between strings and
	 * byte arrays.
	 */
	private static final Charset CHARSET = Charset.forName("UTF-8");


	/**
	 * The original payload data type.
	 */
	private Origin origin;


	/**
	 * The JSON object view.
	 */
	private final JSONObject jsonObject;


	/**
	 * The string view.
	 */
	private final String string;


	/**
	 * The byte array view.
	 */
	private final byte[] bytes;


	/**
	 * The Base64URL view.
	 */
	private final Base64URL base64URL;


	/**
	 * The JWS object view.
	 */
	private final JWSObject jwsObject;


	/**
	 * The signed JWT view.
	 */
	private final SignedJWT signedJWT;


	/**
	 * Converts a byte array to a string using {@link #CHARSET}.
	 *
	 * @param bytes The byte array to convert. May be {@code null}.
	 *
	 * @return The resulting string, {@code null} if conversion failed.
	 */
	private static String byteArrayToString(final byte[] bytes) {

		if (bytes == null) {

			return null;
		}

		return new String(bytes, CHARSET);
	}


	/**
	 * Converts a string to a byte array using {@link #CHARSET}.
	 *
	 * @param string The string to convert. May be {@code null}.
	 *
	 * @return The resulting byte array, {@code null} if conversion failed.
	 */
	private static byte[] stringToByteArray(final String string) {

		if (string == null) {

			return null;
		}

		return string.getBytes(CHARSET);
	}


	/**
	 * Creates a new payload from the specified JSON object.
	 *
	 * @param jsonObject The JSON object representing the payload. Must not
	 *                   be {@code null}.
	 */
	public Payload(final JSONObject jsonObject) {

		if (jsonObject == null) {
			throw new IllegalArgumentException("The JSON object must not be null");
		}

		this.jsonObject = jsonObject;
		string = null;
		bytes = null;
		base64URL = null;
		jwsObject = null;
		signedJWT = null;

		origin = Origin.JSON;
	}


	/**
	 * Creates a new payload from the specified string.
	 *
	 * @param string The string representing the payload. Must not be 
	 *               {@code null}.
	 */
	public Payload(final String string) {

		if (string == null) {
			throw new IllegalArgumentException("The string must not be null");
		}

		jsonObject = null;
		this.string = string;
		bytes = null;
		base64URL = null;
		jwsObject = null;
		signedJWT = null;

		origin = Origin.STRING;
	}


	/**
	 * Creates a new payload from the specified byte array.
	 *
	 * @param bytes The byte array representing the payload. Must not be 
	 *              {@code null}.
	 */
	public Payload(final byte[] bytes) {

		if (bytes == null) {
			throw new IllegalArgumentException("The byte array must not be null");
		}

		jsonObject = null;
		string = null;
		this.bytes = bytes;
		base64URL = null;
		jwsObject = null;
		signedJWT = null;

		origin = Origin.BYTE_ARRAY;
	}


	/**
	 * Creates a new payload from the specified Base64URL-encoded object.
	 *
	 * @param base64URL The Base64URL-encoded object representing the 
	 *                  payload. Must not be {@code null}.
	 */
	public Payload(final Base64URL base64URL) {

		if (base64URL == null) {

			throw new IllegalArgumentException("The Base64URL-encoded object must not be null");
		}

		jsonObject = null;
		string = null;
		bytes = null;
		this.base64URL = base64URL;
		jwsObject = null;
		signedJWT = null;

		origin = Origin.BASE64URL;
	}


	/**
	 * Creates a new payload from the specified JWS object. Intended for
	 * signed then encrypted JOSE objects.
	 *
	 * @param jwsObject The JWS object representing the payload. Must be in
	 *                  a signed state and not {@code null}.
	 */
	public Payload(final JWSObject jwsObject) {

		if (jwsObject == null) {
			throw new IllegalArgumentException("The JWS object must not be null");
		}

		if (jwsObject.getState() == JWSObject.State.UNSIGNED) {
			throw new IllegalArgumentException("The JWS object must be signed");
		}

		jsonObject = null;
		string = null;
		bytes = null;
		base64URL = null;
		this.jwsObject = jwsObject;
		signedJWT = null;

		origin = Origin.JWS_OBJECT;
	}


	/**
	 * Creates a new payload from the specified signed JSON Web Token
	 * (JWT). Intended for signed then encrypted JWTs.
	 *
	 * @param signedJWT The signed JWT representing the payload. Must be in
	 *                  a signed state and not {@code null}.
	 */
	public Payload(final SignedJWT signedJWT) {

		if (signedJWT == null) {
			throw new IllegalArgumentException("The signed JWT must not be null");
		}

		if (signedJWT.getState() == JWSObject.State.UNSIGNED) {
			throw new IllegalArgumentException("The JWT must be signed");
		}

		jsonObject = null;
		string = null;
		bytes = null;
		base64URL = null;
		this.signedJWT = signedJWT;
		jwsObject = signedJWT; // The signed JWT is also a JWS

		origin = Origin.SIGNED_JWT;
	}


	/**
	 * Gets the original data type used to create this payload.
	 *
	 * @return The payload origin.
	 */
	public Origin getOrigin() {

		return origin;
	}


	/**
	 * Returns a JSON object view of this payload.
	 *
	 * @return The JSON object view, {@code null} if the payload couldn't
	 *         be converted to a JSON object.
	 */
	public JSONObject toJSONObject() {

		if (jsonObject != null) {
			return jsonObject;
		}

		// Convert

		String s = toString();

		if (s == null) {
			// to string conversion failed
			return null;
		}

		try {
			return JSONObjectUtils.parseJSONObject(s);

		} catch (ParseException e) {
			// Payload not a JSON object
			return null;
		}
	}


	/**
	 * Returns a string view of this payload.
	 *
	 * @return The string view.
	 */
	@Override
	public String toString() {

		if (string != null) {

			return string;
		}

		// Convert
		if (jwsObject != null) {

			if (jwsObject.getParsedString() != null) {
				return jwsObject.getParsedString();
			} else {
				return jwsObject.serialize();
			}

		} else if (jsonObject != null) {

			return jsonObject.toString();

		} else if (bytes != null) {

			return byteArrayToString(bytes);

		} else if (base64URL != null) {

			return base64URL.decodeToString();
		} else {
			return null; // should never happen
		}
	}


	/**
	 * Returns a byte array view of this payload.
	 *
	 * @return The byte array view.
	 */
	public byte[] toBytes() {

		if (bytes != null) {
			
			return bytes;
		}

		// Convert

		if (base64URL != null) {
			return base64URL.decode();

		}

		return stringToByteArray(toString());
	}


	/**
	 * Returns a Base64URL view of this payload.
	 *
	 * @return The Base64URL view.
	 */
	public Base64URL toBase64URL() {

		if (base64URL != null) {

			return base64URL;
		}

		// Convert

		return Base64URL.encode(toBytes());
	}


	/**
	 * Returns a JWS object view of this payload. Intended for signed then
	 * encrypted JOSE objects.
	 *
	 * @return The JWS object view, {@code null} if the payload couldn't
	 *         be converted to a JWS object.
	 */
	public JWSObject toJWSObject() {

		if (jwsObject != null) {

			return jwsObject;
		}

		try {
			return JWSObject.parse(toString());

		} catch (ParseException e) {

			return null;
		}
	}


	/**
	 * Returns a signed JSON Web Token (JWT) view of this payload. Intended
	 * for signed then encrypted JWTs.
	 *
	 * @return The signed JWT view, {@code null} if the payload couldn't be
	 *         converted to a signed JWT.
	 */
	public SignedJWT toSignedJWT() {

		if (signedJWT != null) {

			return signedJWT;
		}

		try {
			return SignedJWT.parse(toString());

		} catch (ParseException e) {

			return null;
		}
	}
}
