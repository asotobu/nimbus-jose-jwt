/**
 * JSON Web Token (JWT) classes.
 *
 * <p>This package provides representation, compact serialisation and parsing 
 * for the following JWT objects:
 *
 * <ul>
 *     <li>{@link com.nimbusds.jwt.PlainJWT Plain JWTs}.
 *     <li>{@link com.nimbusds.jwt.SignedJWT Signed JWTs}.
 *     <li>{@link com.nimbusds.jwt.EncryptedJWT Encrypted JWTs}.
 * </ul>
 *
 * <p>References:
 *
 * <ul>
 *     <li>http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-32
 * </ul>
 */
package com.nimbusds.jwt;
